import React from 'react';
import { Carousel } from "react-responsive-carousel";
import "./imageCarousel.css";
import { processInfo } from '../../portfolio';

export default function CarouselDisplay({ text, className, href, newTab }) {
  return (
    <Carousel autoplay>
      {processInfo.steps.map((step,i) => {
          return (
            <div key={i} >
              <img alt="" src={step.image} />
                <p className="legend">
                  {step.stepTitle}
                  
                  <ul>
                    {step.bullets.map((bullet,i) => {
                        return (
                          <li key={i} class="text-left">
                            {bullet.text}
                          </li>
                        );
                    })}
                  </ul>
                </p>
            </div>    
          );
      })}
    </Carousel>
  );
}