import React, { useContext } from 'react';
import './Skills.css';
import SoftwareSkill from '../../components/softwareSkills/SoftwareSkill';
import { illustration, skillsSection } from '../../portfolio';
import { Fade, Slide } from 'react-reveal';
import uxDeveloper from '../../assets/lottie/uxDeveloper.json';
import DisplayLottie from '../../components/displayLottie/DisplayLottie';
import StyleContext from '../../contexts/StyleContext';

export default function Skills() {
  const { isDark } = useContext(StyleContext);
  if (!skillsSection.display) {
    return null;
  }
  return (
    <div className={isDark ? 'dark-mode main' : 'main'} id="skills">
      <div className="skills-main-div">
        <Fade left duration={illustration.leftRightAnimationDuration}>
          <div className="skills-text-div">
            <div className={isDark ? 'dark-mode skills-heading' : 'skills-heading'}>
              {skillsSection.title}{' '}
            </div>
            <p className={
                isDark
                  ? 'dark-mode subTitle skills-text-subtitle'
                  : 'subTitle skills-text-subtitle'
              }
            >
              {skillsSection.subTitle}
            </p>
            <SoftwareSkill />
            <div>
              {skillsSection.skills.map((skills, i) => {
                return (
                  <p
                    key={i}
                    className={
                      isDark
                        ? 'dark-mode subTitle skills-text'
                        : 'subTitle skills-text'
                    }
                  >
                    {skills}
                  </p>
                );
              })}
            </div>
          </div>
        </Fade>
        <Fade right duration={illustration.leftRightAnimationDuration}>
          <div className="skills-image-div">
            {illustration.animated ? (
              <DisplayLottie animationData={uxDeveloper} />
            ) : (
              <img
                alt="Man Working"
                src={require('../../assets/images/developerActivity.svg')}
              ></img>
            )}
          </div>
        </Fade>
      </div>
      <Slide left duration={illustration.leftRightAnimationDuration}>
        <div className="main-border"></div>
      </Slide>
    </div>
  );
}
