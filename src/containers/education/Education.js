import React from 'react';
import { Fade } from "react-reveal";
import { illustration } from "../../portfolio";
import './Education.css';
import EducationCard from '../../components/educationCard/EducationCard';
import { education } from '../../portfolio';
import { educationInfo } from '../../portfolio';

export default function Education() {
   if(educationInfo.display){
     return (
      <Fade bottom duration={illustration.leftRightAnimationDuration} distance="40px">
      <div className="education-section" id="education">
        <div className="education-heading">{education.title}</div>
        <div className="education-card-container">
          {educationInfo.schools.map((school) => (
            <EducationCard school={school} />
          ))}
        </div>
         </div>
        </Fade>
    );
  }
  return null;
}
