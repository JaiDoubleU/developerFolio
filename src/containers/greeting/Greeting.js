import React, { useContext } from 'react';
import { Fade, Slide } from 'react-reveal';
import emoji from 'react-easy-emoji';
import './Greeting.css';
import uxZen from '../../assets/lottie/uxZen.json';
import DisplayLottie from '../../components/displayLottie/DisplayLottie';
import SocialMedia from '../../components/socialMedia/SocialMedia';
import Button from '../../components/button/Button';

import { illustration, greeting } from '../../portfolio';
import StyleContext from '../../contexts/StyleContext';

export default function Greeting() {
  const { isDark } = useContext(StyleContext);
  if (!greeting.displayGreeting) {
    return null;
  }
  return (
    <Fade bottom duration={illustration.upAnimationDuration} distance="40px">
      <div className="greet-main main" id="greeting">
        <div className="greeting-main">
          <div className="greeting-text-div">
            <div>
              <div className={isDark ? 'dark-mode greeting-text' : 'greeting-text'}>
                {' '}
                {greeting.title}{' '}
                <span className="wave-emoji">{emoji('👋')}</span>
              </div>
              <p
                className={
                  isDark
                    ? 'dark-mode greeting-text-p'
                    : 'greeting-text-p subTitle'
                }
              >
                {greeting.subTitle}
              </p>
              
              <div className="button-greeting-div">
                <Button text="Contact me" href="#contact" />
                <Button
                  text="See my resume"
                  newTab={true}
                  href={greeting.resumeLink}
                />
              </div>

                <p class="social-media-container">
                  <SocialMedia />
                </p>
            </div>
          </div>
          <div className="greeting-image-div">
            {illustration.animated ? (
              <DisplayLottie animationData={uxZen} />
            ) : (
              <img
                alt="man sitting on table"
                src={require('../../assets/images/manOnTable.svg')}
              ></img>
            )}
          </div>
        </div>

        <Slide left duration={illustration.leftRightAnimationDuration}>
          <div className="main-border"></div>
        </Slide>
      </div>
    </Fade>
  );
}
