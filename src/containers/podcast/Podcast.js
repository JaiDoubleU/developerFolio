import React, { useContext } from "react";
import "./Podcast.css";
import { illustration, podcastSection } from "../../portfolio";
import { Fade } from "react-reveal";
import StyleContext from "../../contexts/StyleContext";

export default function Podcast() {
  const { isDark } = useContext(StyleContext);
  if (!podcastSection.display) {
    return null;
  }
  return (
    <Fade bottom duration={illustration.upAnimationDuration} distance="20px">
      <div className="main">
        <div className="podcast-header">
          <div className="podcast-header-title">{podcastSection.title}</div>
          <p
            className={
              isDark
                ? "dark-mode podcast-header-subtitle"
                : "subTitle podcast-header-subtitle"
            }
          >
            {podcastSection.subtitle}
          </p>
        </div>
        <div className="podcast-main-div">
          {podcastSection.podcast.map((podcastLink,i) => {
            return (
              <div
                key={i}
              >
                <iframe
                  title="podcastIframe"
                  className="podcast"
                  src={podcastLink}
                  frameBorder="0"
                  scrolling="no"
                ></iframe>
              </div>
            );
          })}
        </div>
      </div>
    </Fade>
  );
}
