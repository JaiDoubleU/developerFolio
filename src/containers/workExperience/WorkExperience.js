import React, {useContext} from "react";
import "./WorkExperience.css";
import ExperienceCard from "../../components/experienceCard/ExperienceCard";
import { workExperiences, illustration } from "../../portfolio";
import { Fade, Slide } from "react-reveal";
import StyleContext from "../../contexts/StyleContext";

export default function WorkExperience() {
    const {isDark} = useContext(StyleContext)
    if(workExperiences.display){
        return (
            <div className={isDark ? 'dark-mode main' : 'main'} id="experience">
                <Fade bottom duration={illustration.upAnimationDuration} distance={illustration.animationDistanceSmall}>
                    <div className="main experience-container" id="workExperience">
                        <div>
                            <div className="header-title experience-heading">{workExperiences.title}</div>
                            <p>&nbsp;</p>
                            <div className="experience-cards-div">
                                {workExperiences.experience.map((card,i) => {
                                    return (
                                        <ExperienceCard
                                            key={i}
                                            isDark={isDark}
                                            cardInfo={{
                                                company: card.company,
                                                desc: card.desc,
                                                date: card.date,
                                                companylogo: card.companylogo,
                                                role: card.role,
                                                descBullets: card.descBullets
                                            }}
                                        />
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                </Fade>
                <Slide left duration={illustration.leftRightAnimationDuration}>
                    <div className="main-border"></div>
                </Slide>
            </div>
        );
    }
    return null;
}
