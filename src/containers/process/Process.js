import React from "react";
import { Fade, Slide } from "react-reveal";
import "./Process.css";

import Carousel from "../../components/imageCarousel/imageCarousel";

import { illustration, processSection } from '../../portfolio';

import "react-responsive-carousel/lib/styles/carousel.min.css";


export default function Process() {
  if (!processSection.display) {
    return null;
  }
  return (
    <Fade bottom duration={illustration.upAnimationDuration} distance="20px">
      <div className="main">
        <div className="process-heading">
          <div className="process-heading-title">{processSection.title}</div>
          <p class="process-text-subtitle">          
            {processSection.subtitle}
          </p>
        </div>
        <Carousel />

        <Slide left duration={illustration.leftRightAnimationDuration}>
          <div className="main-border"></div>
        </Slide>
      </div>
    </Fade>
  );
}


