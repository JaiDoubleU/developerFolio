﻿/* Change this file to get your personal Portfolio */

// Summary And Greeting Section

import emoji from 'react-easy-emoji';

const illustration = {
  animated: true, // set to false to use static SVG
  animationDistanceSmall: '20px',
  animationDistance: '30px',
  animationDistanceLarge: '50px',
  upAnimationDuration: 1000,
  leftRightAnimationDuration: 2000
};

const greeting = {
  username: 'Jason Shannon',
  title: "Hi all, I'm Jason",
  subTitle: emoji(
    'I am a User Experience Designer with a passion for delivering delightful User Experiences for Enterprise Web and Mobile applications.'
  ),
  resumeLink:
    'https://drive.google.com/file/d/1ofFdKF_mqscH8WvXkSObnVvC9kK7Ldlu/view?usp=sharing',
  displayGreeting: true, // Set false to hide this section, defaults to true
};

// Social Media Links

const socialMediaLinks = {
  linkedin: 'https://www.linkedin.com/in/JaiDoubleU/',
  gmail: 'jwshannon@gmail.com',
  //github: 'https://github.com/JaiDoubleU',
  gitlab: 'https://gitlab.com/JaiDoubleU',
  medium: 'https://medium.com/@JaiDoubleU',
  //stackoverflow: 'https://stackoverflow.com/users/10422806/saad-pasta',
  //instagram: 'https://instagram.com/JaiDoubleU',
  // Instagram and Twitter are also supported in the links!
  display: true, // Set true to display this section, defaults to false
};

// Skills Section

const skillsSection = {
  title: emoji('👨‍💻 Development Skills'),
  subTitle: 'I bring intuitiveness to Enterprise Web Applications, one delightful experience at a time.',
  skills: [
    emoji(
      '⚡ I am an observer and a curious designer, always interested in learning something new everyday.'
    ),
    //emoji('⚡ Progressive Web Applications ( PWA )'),
  ],

  /* Make Sure to include correct Font Awesome Classname to view your icon
https://fontawesome.com/icons?d=gallery */

  softwareSkills: [
    {
      skillName: 'html-5',
      fontAwesomeClassname: 'fab fa-html5',
    },{
      skillName: 'CSS',
      fontAwesomeClassname: 'fab fa-css3-alt',
    },{
      skillName: 'SASS',
      fontAwesomeClassname: 'fab fa-sass',
    },{
      skillName: 'JavaScript',
      fontAwesomeClassname: 'fab fa-js',
    },{
      skillName: 'Angular',
      fontAwesomeClassname: 'fab fa-angular',
    },{
      skillName: 'NodeJS',
      fontAwesomeClassname: 'fab fa-node',
    },{
      skillName: 'SQL',
      fontAwesomeClassname: 'fas fa-database',
    }
  ],
  display: true, // Set false to hide this section, defaults to true
};

// Process Section

const processSection = {
  title: emoji('🎨 Design Process'),
  subtitle: 'A visual illustration of my process',
  display: true, // Set false to hide this section, defaults to true
};

const processInfo = {
  steps: [
    {
      stepTitle: 'Discovery',
      image: require('./assets/images/process/discovery.png'),
      subTitle: '',
      bullets: [
        { 'text': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit' },
        { 'text': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit' },
      ]
    },
    {
      stepTitle: 'Ideation',
      image: require('./assets/images/process/ideation.png'),
      subTitle: '',
      bullets: [
        { 'text': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit' },
        { 'text': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit' },
      ]
    }
  ]
};

// Education Section

const educationInfo = {
  schools: [
    {
      schoolName: 'University of Calgary', 
      logo: require('./assets/images/UniversityOfCalgaryLogo.png'),
      subHeader: '',
      duration: 'Septtember 2017 - April 2019',
      desc: 'Participated in the research of XXX and published 3 papers.',
      descBullets: [
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      ],
    }
  ],
  display: true // Set false to hide this section, defaults to true
};


const education = {
  title: emoji('🎓 Education'),
  subtitle: '',
  display: false, // Set false to hide this section, defaults to true
};

// Your top 3 proficient stacks/tech experience
const techStack = {
  viewSkillBars: false, //Set it to true to show Proficiency Section
  experience: [
    {
      Stack: 'User Experience Design', //Insert stack or technology you have experience in
      progressPercentage: '90%' //Insert relative proficiency in percentage
    },
    {
      Stack: 'CSS',
      progressPercentage: '90%'
    },
    {
      Stack: 'JavaScript',
      progressPercentage: '80%'
    },
    {
      Stack: 'SASS',
      progressPercentage: '80%'
    },
    {
      Stack: 'Angular',
      progressPercentage: '80%'
    },
  ],
  displayCodersrank: false, // Set true to display codersrank badges section need to changes your username in src/containers/skillProgress/skillProgress.js:17:62, defaults to false
};

// Work experience section

const workExperiences = {
  display: true, //Set it to true to show workExperiences Section
  title: emoji('💼  Work Experience'),
  experience: [
    {
      role: 'Senior User Experience Designer',
      company: 'Enverus',
      companylogo: require('./assets/images/enverusLogo.png'), 
      date: 'Aug 2019 – Present',
      desc:'',
      descBullets: [
        // 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
        // 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      ],
    },
    {
      role: 'Senior User Experience Designer',
      company: 'DrillingInfo',
      companylogo: require('./assets/images/drillingInfoLogo.png'), 
      date: 'Sept 2019 – Aug 2019',
      desc:
        '',
      descBullets: [
        // 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
        // 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      ],
    },
    {
      role: 'Senior User Experience Designer',
      company: 'Oildex',
      companylogo: require('./assets/images/oildexLogo.png'),
      date: 'Jun 2015 – Sept 2018',
      desc:
        '',
    },
    {
      role: 'Senior User Experience Designer',
      company: 'ADP',
      companylogo: require('./assets/images/adpLogo.png'),
      date: 'Jan 2010 – Jun 2015',
      desc:
        '',
    },
    {
      role: 'User Experience Designer',
      company: 'Pros4Hire.com',
      companylogo: require('./assets/images/pros4HireLogo.png'),
      date: 'Jan 2010 – Jun 2012',
      desc:
        '',
    },
    {
      role: 'Web Designer / Board of Directors',
      company: 'CHADD Canada',
      companylogo: require('./assets/images/CHADDLogo.png'),
      date: 'Jan 2004 – Jun 2012',
      desc:
        '',
    },
    {
      role: 'User Interface Developer',
      company: 'Digital Oilfield',
      companylogo: require('./assets/images/digitalOilfieldLogo.png'),
      date: 'Sept 2005 – Jun 2010',
      desc:
        '',
    },
    {
      role: 'Software Developer',
      company: 'Securac',
      companylogo: require('./assets/images/securac.png'),
      date: 'Sept 2004 – Sept 2005',
      desc:
        '',
    },
    {
      role: 'Software Developer',
      company: 'Vault Technologies',
      companylogo: require('./assets/images/vaultLogo.png'),
      date: 'Jan 2003 – Sept 2004',
      desc:
        '',
    },
    {
      role: 'Software Developer',
      company: 'EFA Software',
      companylogo: require('./assets/images/efaLogo.png'),
      date: 'Sept 2000 – Dec 2002',
      desc:
        '',
    },
    {
      role: 'Software Developer/Analyst',
      company: 'TransCanada Pipelines',
      companylogo: require('./assets/images/transcanadaLogo.png'),
      date: 'Jan 1998 – Sept 2000',
      desc:
        '',
    },
    {
      role: 'Inventory Control Coordinator',
      company: 'McKesson',
      companylogo: require('./assets/images/mckessonLogo.png'),
      date: 'Feb 1995 – Aug 1997',
      desc:
        '',
    },
    {
      role: 'Client Services Representative',
      company: 'Bristol Meyers Squibb',
      companylogo: require('./assets/images/bristolMyersLogo.png'),
      date: 'Jan 1992 – Jan 1995',
      desc:
        '',
    }
  ], 
};

/* Your Open Source Section to View Your Github Pinned Projects
To know how to get github key look at readme.md */

const openSource = {
  githubConvertedToken: process.env.REACT_APP_GITHUB_TOKEN,
  githubUserName: 'JaiDoubleU', // Change to your github username to view your profile in Contact Section.
  showGithubProfile: 'false', // Set true or false to show Contact profile using Github, defaults to true
  display: false, // Set false to hide this section, defaults to true
};

// Some big projects you have worked on

const bigProjects = {
  title: 'Big Projects',
  subtitle: 'SOME STARTUPS AND COMPANIES THAT I HELPED TO CREATE THEIR TECH',
  display: false, // Set false to hide this section, defaults to true
  projects: [
    {
      image: require('./assets/images/saayaHealthLogo.webp'),
      link: 'http://saayahealth.com/',
    },
    {
      image: require('./assets/images/nextuLogo.webp'),
      link: 'http://nextu.se/',
    },
  ],
};

// Achievement Section
// Include certificates, talks etc

const achievementSection = {
  title: emoji('🏆 Achievements And Certifications'),
  subtitle:
    'Achievements, Certifications, Award Letters and Some Cool Stuff that I have done !',
  display: true, // Set false to hide this section, defaults to true
  achievementsCards: [
    {
      title: 'Google Code-In Finalist',
      subtitle:
        'First Pakistani to be selected as Google Code-in Finalist from 4000 students from 77 different countries.',
      image: require('./assets/images/codeInLogo.webp'),
      footerLink: [
        {
          name: 'Certification',
          url:
            'https://drive.google.com/file/d/0B7kazrtMwm5dYkVvNjdNWjNybWJrbndFSHpNY2NFV1p4YmU0/view?usp=sharing',
        },
        {
          name: 'Award Letter',
          url:
            'https://drive.google.com/file/d/0B7kazrtMwm5dekxBTW5hQkg2WXUyR3QzQmR0VERiLXlGRVdF/view?usp=sharing',
        },
        {
          name: 'Google Code-in Blog',
          url:
            'https://opensource.googleblog.com/2019/01/google-code-in-2018-winners.html',
        },
      ],
    },
    {
      title: 'Google Assistant Action',
      subtitle:
        'Developed a Google Assistant Action JavaScript Guru that is available on 2 Billion devices world wide.',
      image: require('./assets/images/googleAssistantLogo.webp'),
      footerLink: [
        {
          name: 'View Google Assistant Action',
          url:
            'https://assistant.google.com/services/a/uid/000000100ee688ee?hl=en',
        },
      ],
    },

    {
      title: 'PWA Web App Developer',
      subtitle: 'Completed Certifcation from SMIT for PWA Web App Development',
      image: require('./assets/images/pwaLogo.webp'),
      footerLink: [
        { name: 'Certification', url: '' },
        {
          name: 'Final Project',
          url: 'https://pakistan-olx-1.firebaseapp.com/',
        },
      ],
    },
  ],
};

// Blogs Section

const blogSection = {
  title: 'Blogs',
  subtitle: 'With Love for Developing cool stuff, I love to write and teach others what I have learnt.',
  display: false, // Set false to hide this section, defaults to true
  blogs: [
    {
      url:
        'https://blog.usejournal.com/create-a-google-assistant-action-and-win-a-google-t-shirt-and-cloud-credits-4a8d86d76eae',
      title: 'Win a Google Assistant Tshirt and $200 in Google Cloud Credits',
      description:
        'Do you want to win $200 and Google Assistant Tshirt by creating a Google Assistant Action in less then 30 min?',
    },
    {
      url: 'https://medium.com/@JaiDoubleU/why-react-is-the-best-5a97563f423e',
      title: 'Why REACT is The Best?',
      description:
        'React is a JavaScript library for building User Interface. It is maintained by Facebook and a community of individual developers and companies.',
    },
  ],
};

// Talks Sections

const talkSection = {
  title: 'TALKS',
  subtitle: emoji(
    'I LOVE TO SHARE MY LIMITED KNOWLEDGE AND GET A SPEAKER BADGE 😅'
  ),
  display: false, // Set false to hide this section, defaults to true
  talks: [
    {
      title: 'Build Actions For Google Assistant',
      subtitle: 'Codelab at GDG DevFest Karachi 2019',
      slides_url: 'https://bit.ly/JaiDoubleU-slides',
      event_url: 'https://www.facebook.com/events/2339906106275053/',
    },
  ],
};

// Podcast Section

const podcastSection = {
  title: emoji('Podcast 🎙️'),
  subtitle: 'I LOVE TO TALK ABOUT MYSELF AND TECHNOLOGY',
  display: false, // Set false to hide this section, defaults to true
  // Please Provide with Your Podcast embeded Link
  podcast: [
    '',
  ],
};

const contactInfo = {
  title: emoji('☎️ Contact Me'),
  subtitle: 'Discuss a project or just want to say hi?  Lets chat.',
  number: '403-922-6361',
  emailAddress: 'jwshannon@gmail.com',
};

// Twitter Section

const twitterDetails = {
  userName: 'twitter', //Replace "twitter" with your twitter username without @
  display: false, // Set true to display this section, defaults to false
};

export {
  illustration,
  greeting,
  socialMediaLinks,
  skillsSection,
  education,
  processSection,
  processInfo,
  educationInfo,
  techStack,
  workExperiences,
  openSource,
  bigProjects,
  achievementSection,
  blogSection,
  talkSection,
  podcastSection,
  contactInfo,
  twitterDetails,
};
